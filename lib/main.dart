import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.cyan[800],
        appBar: AppBar(
          backgroundColor: Colors.cyan[900],
          title: const Text(
            'Jogando Dados',
            style: TextStyle(
              fontFamily: 'Noto Serif - Bold',
              fontWeight: FontWeight.bold,
              fontSize: 25.0,
              color: Colors.white,
              letterSpacing: 1.0,
            ),
          ),
        ),
        body: const Dados(),
      ),
    );
  }
}

class Dados extends StatefulWidget {
  const Dados({super.key});

  @override
  State<Dados> createState() => _DadosState();
}

class _DadosState extends State<Dados> {
  int dadoEsquerdo = 1;
  int dadoDireito = 1;

  rodarDados() {
    setState(() {
      dadoDireito = Random().nextInt(6) + 1;
      dadoEsquerdo = Random().nextInt(6) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: [
          Expanded(
            child: TextButton(
              onPressed: () {
                if (kDebugMode) {
                  rodarDados();
                }
              },
              child: Image.asset('assets/image/dado$dadoEsquerdo.png'),
            ),
          ),
          Expanded(
            child: TextButton(
              onPressed: () {
                if (kDebugMode) {
                  rodarDados();
                }
              },
              child: Image.asset('assets/image/dado$dadoDireito.png'),
            ),
          ),
        ],
      ),
    );
  }
}
